package com.alexk8s.spective.server.collector;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.RedditPostRepository;
import com.alexk8s.spective.server.utility.FluxPersistentSubscriptionListener;
import com.eventstore.dbclient.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import redis.clients.jedis.CommandArguments;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPooled;
import redis.clients.jedis.bloom.BFInsertParams;
import redis.clients.jedis.bloom.RedisBloomProtocol;
import redis.clients.jedis.bloom.commands.RedisBloomCommands;
import redis.clients.jedis.commands.ProtocolCommand;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile("redis-post-id-collector")
public class RedisPostIdCollector {

    private final EventStoreDBClientSettings settings;

    private Disposable fluxSubscription;
    private PersistentSubscription persistentSubscription;
    private EventStoreDBPersistentSubscriptionsClient dbClient;
    private final JedisPooled jedisPooled;

    private static final String STREAM_NAME = "reddit-posts", GROUP_NAME = "redis-post-id-collector";


    @PostConstruct
    public void configure() {
        dbClient = EventStoreDBPersistentSubscriptionsClient.create(settings);
        try {
            if (dbClient.getInfoToStream(STREAM_NAME, GROUP_NAME).get().isEmpty()) {
                dbClient.createToStream(STREAM_NAME, GROUP_NAME, CreatePersistentSubscriptionToStreamOptions.get().fromStart());
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    @SneakyThrows
    public void shutdown() {
        if (fluxSubscription != null)
            fluxSubscription.dispose();
        if (persistentSubscription != null)
            persistentSubscription.stop();
        if (dbClient != null) {
            try {
                dbClient.shutdown();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        fluxSubscription = Flux.create(this::initializeSubscription)
                .transform(this::collect)
                .subscribe();
    }

    private void initializeSubscription(FluxSink<ResolvedEvent> sink) {
        SubscribePersistentSubscriptionOptions options = SubscribePersistentSubscriptionOptions.get().bufferSize(1000);
        persistentSubscription = dbClient.subscribeToStream(STREAM_NAME, GROUP_NAME, options, new FluxPersistentSubscriptionListener(sink)).join();
    }

    private Publisher<?> collect(Flux<ResolvedEvent> flux) {
        BFInsertParams insertParams = BFInsertParams.insertParams().error(.000001).capacity(300_000_000).expansion(2);
        return flux
                .bufferTimeout(100, Duration.ofMillis(100))
                .doOnNext(e->jedisPooled.bfInsert("reddit_ids_bf",insertParams,parseIds(e)))
                .doOnNext(x -> persistentSubscription.ack(x.iterator()))
                .doOnError(x -> log.error("Error with subscription processing ", x));

    }

    private String[] parseIds(List<ResolvedEvent> events) {
        return  events.stream().map(e -> parseEvent(e).getId()).toArray(String[]::new);
    }

    @SneakyThrows
    private RedditPostModel parseEvent(ResolvedEvent event) {
        return event.getEvent().getEventDataAs(RedditPostModel.class);
    }

}
