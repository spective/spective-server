package com.alexk8s.spective.server.collector;

import com.alexk8s.spective.server.service.Reddit4JService;
import com.eventstore.dbclient.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.client.Reddit4J;
import masecla.reddit4j.objects.RedditPost;
import masecla.reddit4j.objects.Sorting;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.LongStream;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile("id-collector")
public class IdCollector {
    private final Reddit4JService reddit4JService;
    private Reddit4J client;
    private final EventStoreDBClient eventStoreDBClient;
    private long lastCheckpoint = -1;

    private final String STREAM_NAME = "reddit-ids";
    @PostConstruct
    public void init() {
        try{

        StreamMetadata streamMetadata = new StreamMetadata();
        streamMetadata.setMaxCount(100_000_000);
        eventStoreDBClient.setStreamMetadata(STREAM_NAME,streamMetadata);
        } catch (RuntimeException e){
            log.warn("Error configuring reddit ids stream size");
        }
        try{

        ReadResult readResult = eventStoreDBClient.readStream(STREAM_NAME, ReadStreamOptions.get().fromEnd().backwards().maxCount(1))
                .join();
        readResult.getEvents().stream().findFirst()
                .map(this::parseEventCheckpoint)
                .ifPresent(l->lastCheckpoint=l);
        } catch (RuntimeException e){
            log.info("Error reading first result off of stream",e);
        }
        client = reddit4JService.getReddit4J();
    }

    @SneakyThrows
    private Long parseEventCheckpoint(ResolvedEvent resolvedEvent){
        Map eventDataAs = resolvedEvent.getEvent().getEventDataAs(Map.class);
        log.info("Using checkpoint event {}",eventDataAs);
        return  Long.parseLong(eventDataAs.get("idValue").toString()) ;
    }

    @Scheduled(fixedRate = 1,timeUnit = TimeUnit.HOURS)
    public void resetClient(){
        client = reddit4JService.getReddit4J();
    }

    @Scheduled(fixedRate = 2000)
    @SneakyThrows
    public void collect() {
        List<RedditPost> posts = client.getSubredditPosts("all", Sorting.NEW).submit();
        if (posts.isEmpty()) {
            log.warn("No response available from new posts");
            return;
        }
        RedditPost redditPost = posts.get(0);
        String id = redditPost.getId();
        long idValue = Long.parseLong(id, 36);
        if (lastCheckpoint != -1) {
            log.info("{} - {} Loading {}", id, idValue, idValue - lastCheckpoint);
            for (long i = lastCheckpoint; i < idValue; i += 10_000) {
                long upperBound = Math.min(i + 10_000, idValue + 1);
                log.debug("Pushing {} - {}", i, upperBound);
                Iterator<EventData> iterator = LongStream.range(i, upperBound).mapToObj(

                        m -> EventData.builderAsJson("reddit-id", Map.of("id", Long.toString(m, 36), "idValue", m)).build()
                ).iterator();
                eventStoreDBClient.appendToStream("reddit-ids", iterator).get();
            }
        } else {

            log.info("{} - {}", id, idValue);
        }
        lastCheckpoint = idValue;
    }
}
