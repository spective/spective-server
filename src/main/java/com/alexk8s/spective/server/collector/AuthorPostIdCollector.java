package com.alexk8s.spective.server.collector;

import com.alexk8s.spective.server.model.RedditPostId;
import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.RedditPostIdRepository;
import com.eventstore.dbclient.EventStoreDBClientSettings;
import com.eventstore.dbclient.ResolvedEvent;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("reddit-post-id-collector")
public class AuthorPostIdCollector extends RedditPostIdCollector{

    @SneakyThrows
    public AuthorPostIdCollector(EventStoreDBClientSettings settings, RedditPostIdRepository redditPostIdRepository) {
        super(settings, redditPostIdRepository);
    }

    @Override
    String getGroupName() {
        return "author-post-id-collector";
    }

    @Override
    RedditPostId parseEvent(ResolvedEvent resolvedEvent) {
        RedditPostModel redditPostModel = parseRedditPost(resolvedEvent);
        return new RedditPostId("author",redditPostModel.getAuthor(),redditPostModel.getCreatedUtc(),redditPostModel.getId());
    }
}
