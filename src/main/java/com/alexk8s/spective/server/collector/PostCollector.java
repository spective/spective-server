package com.alexk8s.spective.server.collector;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.service.Reddit4JFactory;
import com.alexk8s.spective.server.service.Reddit4JService;
import com.alexk8s.spective.server.service.RedditService;
import com.alexk8s.spective.server.utility.FluxPersistentSubscriptionListener;
import com.eventstore.dbclient.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.client.Reddit4J;
import masecla.reddit4j.objects.RedditData;
import masecla.reddit4j.objects.RedditListing;
import masecla.reddit4j.objects.RedditPost;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.reactivestreams.Publisher;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import redis.clients.jedis.JedisPooled;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.naming.spi.ResolveResult;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile("post-collector")
public class PostCollector {
    private final RedditService redditService;
    private final EventStoreDBClientSettings settings;
    private final EventStoreDBClient eventStoreDBClient;
    private PersistentSubscription persistentSubscription;
    private Disposable fluxSubscription;
    private EventStoreDBPersistentSubscriptionsClient dbClient;
    private final JedisPooled jedisPooled;

    private static final String STREAM_NAME = "reddit-ids", GROUP_NAME = "post-collector", REDDIT_POST_STREAM = "reddit-posts";

    @PostConstruct
    public void configure() {
        StreamMetadata streamMetadata = new StreamMetadata();
        streamMetadata.setMaxCount(50_000_000);
        eventStoreDBClient.setStreamMetadata(REDDIT_POST_STREAM, streamMetadata);
        dbClient = EventStoreDBPersistentSubscriptionsClient.create(settings);
        try {
            if (dbClient.getInfoToStream(STREAM_NAME, GROUP_NAME).get().isEmpty()) {
                dbClient.createToStream(STREAM_NAME, GROUP_NAME, CreatePersistentSubscriptionToStreamOptions.get().fromStart());
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        fluxSubscription = Flux.create(this::initializeSubscription)
                .transform(this::collect).subscribe();

    }


    @PreDestroy
    public void shutdown() {
        fluxSubscription.dispose();
        persistentSubscription.stop();
    }

    private void initializeSubscription(FluxSink<ResolvedEvent> sink) {
        SubscribePersistentSubscriptionOptions options = SubscribePersistentSubscriptionOptions.get().bufferSize(500);
        persistentSubscription = dbClient.subscribeToStream(STREAM_NAME, GROUP_NAME, options, new FluxPersistentSubscriptionListener(sink)).join();
    }

    private Publisher<?> collect(Flux<ResolvedEvent> flux) {
        return flux.bufferTimeout(100, Duration.of(1, ChronoUnit.SECONDS))
                .flatMap(this::parseEventsMono)
                .doOnNext(x -> persistentSubscription.ack(x.iterator()))
                .doOnError(x -> log.error("Error with subscription processing ", x));
    }

    private Mono<List<ResolvedEvent>> parseEventsMono(List<ResolvedEvent> events) {
        CompletableFuture<WriteResult> future = CompletableFuture.supplyAsync(() -> parseEvents(events));
        return Mono.fromFuture(future)
                .thenReturn(events)
                .doOnError(t -> log.error("Error parsing events", t))
                .onErrorComplete();
    }


    @SneakyThrows
    private WriteResult parseEvents(List<ResolvedEvent> events) {
        List<String> ids = events.stream().filter(e->e!=null && e.getEvent()!=null).map(this::parseEventRedditPostId).collect(Collectors.toList());
        List<EventData> eventData = getPosts(ids).stream().map(this::toEvent).toList();
        return eventStoreDBClient.appendToStream(REDDIT_POST_STREAM, eventData.iterator()).get();
    }

    @SneakyThrows
    private String parseEventRedditPostId(ResolvedEvent event) {
        Map<String, String> map = event.getEvent().getEventDataAs(Map.class);
        return map.get("id");

    }

    public List<String> resolveNewIds(List<String> allIDs) {
        if(allIDs.isEmpty())
            return Collections.emptyList();
        List<Boolean> existsResults = jedisPooled.bfMExists("reddit_ids_bf", allIDs.toArray(new String[0]));
        List<String> newIds = new ArrayList<>();
        for (int i = 0; i < allIDs.size(); i++) {
            if (!existsResults.get(i))
                newIds.add(allIDs.get(i));
        }
        if (allIDs.size() - newIds.size() > 10)
            log.warn("Only {} of {} ids were new", newIds.size(), allIDs.size());
        return newIds;
    }

    @SneakyThrows
    public List<RedditPostModel> getPosts(List<String> ids) {
        if (ids.size() > 100) {
            throw new RuntimeException("list of ids must be less than 100 ");
        }
        List<String> newIds = resolveNewIds(ids);
        if (newIds.isEmpty())
            return Collections.emptyList();
        List<RedditPostModel> redditPosts = redditService.getRedditPosts(ids);
        jedisPooled.bfMAdd("reddit_ids_bf", newIds.toArray(new String[0]));
        return redditPosts;
    }



    private EventData toEvent(RedditPostModel redditPost) {
        return EventDataBuilder.json("reddit-post", redditPost).build();
    }
}
