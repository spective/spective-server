package com.alexk8s.spective.server.collector;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.RedditPostRepository;
import com.alexk8s.spective.server.utility.FluxPersistentSubscriptionListener;
import com.eventstore.dbclient.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.awt.desktop.AppReopenedEvent;
import java.time.Duration;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile("cassandra-post-collector")
public class CassandraPostCollector {

    private final EventStoreDBClientSettings settings;
    private final RedditPostRepository repository;

    private Disposable fluxSubscription;
    private PersistentSubscription persistentSubscription;
    private EventStoreDBPersistentSubscriptionsClient dbClient;

    private static final String STREAM_NAME = "reddit-posts", GROUP_NAME = "cassandra-post-collector-local";


    @PostConstruct
    public void configure() {
        dbClient = EventStoreDBPersistentSubscriptionsClient.create(settings);
        try {
            if (dbClient.getInfoToStream(STREAM_NAME, GROUP_NAME).get().isEmpty()) {
                dbClient.createToStream(STREAM_NAME, GROUP_NAME, CreatePersistentSubscriptionToStreamOptions.get().fromStart());
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    @SneakyThrows
    public void shutdown() {
        if (fluxSubscription != null)
            fluxSubscription.dispose();
        if (persistentSubscription != null)
            persistentSubscription.stop();
        if (dbClient != null) {
            try {
                dbClient.shutdown();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        fluxSubscription = Flux.create(this::initializeSubscription)
                .transform(this::collect)
                .subscribe();
    }

    private void initializeSubscription(FluxSink<ResolvedEvent> sink) {
        SubscribePersistentSubscriptionOptions options = SubscribePersistentSubscriptionOptions.get().bufferSize(1000);
        persistentSubscription = dbClient.subscribeToStream(STREAM_NAME, GROUP_NAME, options, new FluxPersistentSubscriptionListener(sink)).join();
    }

    private Publisher<?> collect(Flux<ResolvedEvent> flux) {
        return flux
                .flatMap(e -> repository.save(parseEvent(e)).retry(3).thenReturn(e))
                .bufferTimeout(100, Duration.ofSeconds(10))
                .doOnNext(x -> persistentSubscription.ack(x.iterator()))
                .doOnError(x -> log.error("Error with subscription processing ", x));

    }

    @SneakyThrows
    private RedditPostModel parseEvent(ResolvedEvent event) {
        RedditPostModel post = event.getEvent().getEventDataAs(RedditPostModel.class);
        post.setSubreddit(post.getSubreddit().toLowerCase());
        post.setAuthor(post.getAuthor().toLowerCase());
        return post;
    }

}
