package com.alexk8s.spective.server.repository;

import com.alexk8s.spective.server.model.RedditPostId;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;

public interface RedditPostIdRepository extends ReactiveCassandraRepository<RedditPostId,String> {
    Flux<RedditPostId> findTop10ByTypeAndValueAndCreatedUtcLessThanOrderByCreatedUtcDesc(String type, String value, Long createdUtc);
}
