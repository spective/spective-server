package com.alexk8s.spective.server.repository;


import com.alexk8s.spective.server.resolver.ResolverConfiguration;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResolverConfigurationRepository extends ReactiveCassandraRepository<ResolverConfiguration,String> {
}
