package com.alexk8s.spective.server.repository;

import com.alexk8s.spective.server.resolver.MediaMetadata;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MediaMetadataRepository extends ReactiveCassandraRepository<MediaMetadata,String> {
    Mono<MediaMetadata> findByPostedUrlAndOriginalUrl(String postedUrl, String originalUrl);
    Flux<MediaMetadata> findByHash(String hash);
    Flux<MediaMetadata> findByImageHash(String imageHash);
}
