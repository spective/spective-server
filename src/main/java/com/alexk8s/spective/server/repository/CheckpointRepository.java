package com.alexk8s.spective.server.repository;

import com.alexk8s.spective.server.model.Checkpoint;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CheckpointRepository extends ReactiveCassandraRepository<Checkpoint,String > {
    Mono<Checkpoint> findFirstByProcessOrderByCheckpointTimeDesc(String process);
}
