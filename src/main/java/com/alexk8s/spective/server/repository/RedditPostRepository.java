package com.alexk8s.spective.server.repository;

import com.alexk8s.spective.server.model.RedditPostModel;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface RedditPostRepository extends ReactiveCassandraRepository<RedditPostModel,String> {
}
