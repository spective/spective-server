package com.alexk8s.spective.server;

import com.alexk8s.spective.server.collector.PostCollector;
import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.HashCacheRepository;
import com.alexk8s.spective.server.repository.MediaMetadataRepository;
import com.alexk8s.spective.server.repository.RedditPostRepository;
import com.alexk8s.spective.server.resolver.HashCache;
import com.alexk8s.spective.server.resolver.MediaMetadata;
import com.alexk8s.spective.server.service.ContentService;
import com.alexk8s.spective.server.utility.MonoContinuation;
import com.eventstore.dbclient.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.appwrite.Client;
import io.appwrite.Permission;
import io.appwrite.Role;
import io.appwrite.models.Document;
import io.appwrite.services.Databases;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.objects.RedditPost;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.buffer.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static com.alexk8s.spective.server.utility.AppwriteUtility.APPWRITE_ENDPOINT;
import static com.alexk8s.spective.server.utility.AppwriteUtility.APPWRITE_PROJECT;

@SpringBootApplication(exclude = {
})
@EnableScheduling
@Slf4j
public class RedditIdCollectorApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(RedditIdCollectorApplication.class, args);
//        testPost(context);
//        loadHashCaches(context);

    }





    @SneakyThrows
    private static void savePosts() {
        String token = "b122593f098a93c59f312f9a3bd073412f0eedddc0aa7bd9915de9f0f935d7a27de65c541a9796a5d022478c44ea260ce7a93e627be355337888e2ecb9c7a7ccee683fefe016b58555986fddc313e6bcacad0c8282b3b365e136d1d674912703c8052782d7ed25b26c5cd5d51fd3ac8c229da4ac92e8b60a050f7b263662ef36";
        String databaseId = "6362ff97310a83da4aa2";
        String collectionId = "6385e06182cebaf259fe";
        Client client = new Client(APPWRITE_ENDPOINT).setProject(APPWRITE_PROJECT).setKey(token);
        Databases databases = new Databases(client);
//        MonoContinuation<DocumentList> existingPostsSink = new MonoContinuation<>();
//        Mono<DocumentList> existingPostsMono = Mono.create(existingPostsSink::setMonoSink);
//        databases.listDocuments(databaseId,collectionId,existingPostsSink);
//        List<Document> existingPosts = existingPostsMono.block().component2();
//        log.info("loaded {} existing posts",existingPosts.size());
//        for (Document existingPost : existingPosts) {
//            MonoContinuation<Object> completion = new MonoContinuation<>();
//            Mono<Object> objectMono = Mono.create(completion::setMonoSink);
//            databases.deleteDocument(databaseId,collectionId,existingPost.getId(), completion);
//            objectMono.block();
//            log.info("Deleted {}",existingPost.getId());
//        }
        List<RaterSubmission> submissions = getSubmissions();
        for (int i = 0; i < submissions.size(); i++) {
            MonoContinuation<Document> objectMonoContinuation = new MonoContinuation<>();
            Mono<Document> documentMono = Mono.create(objectMonoContinuation::setMonoSink);
            String user = Role.Companion.user("63630390421461f2d300", "");

            List<String> permissions = List.of(Permission.Companion.read(user), Permission.Companion.delete(user), Permission.Companion.update(user));
            RaterSubmission data = submissions.get(i);
            if (data.getTitle().length() > 500)
                data.setTitle(data.getTitle().substring(0, 500));
            if (data.getHash() != null && data.getHash().length() > 99) {
                log.warn("Bad post {}", data);
            } else {

                databases.createDocument(databaseId, collectionId, "unique()", data, permissions, objectMonoContinuation);
                documentMono.block();
                log.info("Saved {} posts", i);
            }
        }

    }

    @SneakyThrows
    private static void loadHashCaches(ConfigurableApplicationContext context) {

        HashCacheRepository repository = context.getBean(HashCacheRepository.class);
        Flux.fromStream(Files.lines(Path.of("hashCaches.json")))
                .map(RedditIdCollectorApplication::parseHashCache)
                .flatMap(repository::save, 100)
                .blockLast();
        context.close();
    }

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.findAndRegisterModules();
    }

    @SneakyThrows
    private static HashCache parseHashCache(String line) {
        return OBJECT_MAPPER.readValue(line, HashCache.class);
    }


    private static void testPost(ConfigurableApplicationContext context) { //incomplete todo string analysis
        PostCollector postCollector = context.getBean(PostCollector.class);
//        List<RedditPost> z62ul7 = postCollector.getPosts(List.of("z62ul7"));
//        log.info(z62ul7.toString());
        context.close();
    }

    @SneakyThrows
    private static void cleanPreloads(ConfigurableApplicationContext context) { //incomplete todo recreate hashcache table for timestamp changes
        Set<String> preloads = new HashSet<>(Files.readAllLines(Path.of("preloadlist.txt")));
        Stream<String> needingUpdates = Files.lines(Path.of("hashCaches.json")).map(RedditIdCollectorApplication::decode)
                .filter(x -> needsToggle(x, preloads))
                .map(HashCache::getUrl);

        HashCacheRepository repository = context.getBean(HashCacheRepository.class);
        Flux.fromStream(needingUpdates)
                .flatMap(u -> togglePreload(repository, u), 100)
                .doOnComplete(context::close)
                .subscribe();
    }

    private static Mono<Void> togglePreload(HashCacheRepository repository, String url) {
        return repository.findById(url)
                .doOnNext(x -> {
                    boolean preloaded = x.getPreloaded() != null && x.getPreloaded();
                    x.setPreloaded(!preloaded);
                })
                .flatMap(repository::save)
                .then();
    }

    private static boolean needsToggle(HashCache hashCache, Set<String> preloads) {
        boolean hasHash = preloads.contains(hashCache.getHash()) || preloads.contains(hashCache.getImageHash());
        if (hashCache.getPreloaded() != null && hashCache.getPreloaded()) {
            return !hasHash;
        }
        return hasHash;
    }

    @SneakyThrows
    private static void deletePreload(String preload) {
        String basePath = "/media/spective/spective/preload";
        String newPath = basePath + "/" + preload.charAt(0) + "/" + preload.substring(0, 2) + "/" + preload;
        Files.deleteIfExists(Path.of(newPath));
        log.info("Deleting {}", preload);
    }

    @SneakyThrows
    private static List<RaterSubmission> getSubmissions() {
        RaterSubmission[] raterSubmissions = OBJECT_MAPPER.readValue(new File("submission.json"), RaterSubmission[].class);
        return List.of(raterSubmissions);
    }

    @SneakyThrows
    private static HashCache decode(String json) {
        return OBJECT_MAPPER.readValue(json, HashCache.class);
    }


    private static final DefaultDataBufferFactory DATA_BUFFER_FACTORY = new DefaultDataBufferFactory();

    @Data
    private static class RaterSubmission {
        private String title, poster, subreddit, url, originalUrl, postedUrl, hash, redditPost, type, origin, originId, imageHash;
//       private Long size;
    }

    @SneakyThrows
    private static void loadPreloadToEventstore(ConfigurableApplicationContext context) {

        EventStoreDBClientSettings settings = context.getBean(EventStoreDBClientSettings.class);
        EventStoreDBPersistentSubscriptionsClient dbClient = EventStoreDBPersistentSubscriptionsClient.create(settings);
//        dbClient.createToStream("preloaded-files5", "rename", CreatePersistentSubscriptionToStreamOptions.get().fromStart()).join();
        SubscribePersistentSubscriptionOptions options = SubscribePersistentSubscriptionOptions.get().bufferSize(100);
        RenameListener listener = new RenameListener();
        dbClient.subscribeToStream("preloaded-files5", "rename", options, listener).join();

    }

    private static class RenameListener extends PersistentSubscriptionListener {
        @Override
        @SneakyThrows
        public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent event) {
            String fileName = event.getEvent().getEventDataAs(String.class);
            try {
                String basePath = "/media/spective/spective/preload";
                String currentPath = basePath + "/" + fileName;
                String newPath = basePath + "/" + fileName.charAt(0) + "/" + fileName.substring(0, 2) + "/" + fileName;
                Files.move(Path.of(currentPath), Path.of(newPath));
                log.info("Moved {} -> {}", currentPath, newPath);
                subscription.ack(event);
            } catch (Exception e) {
                log.error("Error moving file {}", fileName, e);
            }
        }

    }


}
