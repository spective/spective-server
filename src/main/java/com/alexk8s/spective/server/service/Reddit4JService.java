package com.alexk8s.spective.server.service;

import lombok.SneakyThrows;
import masecla.reddit4j.client.Reddit4J;
import masecla.reddit4j.client.UserAgentBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class Reddit4JService {
    private final String clientId;
    private final String clientSecret;

    public Reddit4JService(@Value("${REDDIT_CLIENT_ID}") String clientId, @Value("${REDDIT_CLIENT_SECRET}") String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    @SneakyThrows
    public Reddit4J getReddit4J() {
        int i = new Random().nextInt();
        Reddit4J client = Reddit4J.rateLimited()
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setUserAgent(new UserAgentBuilder().appname("spective-"+i).author("me").version("v0.1"));
        client.userlessConnect();
        return client;
    }
}
