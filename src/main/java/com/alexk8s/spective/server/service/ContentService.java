package com.alexk8s.spective.server.service;

import com.alexk8s.spective.server.repository.MediaMetadataRepository;
import com.alexk8s.spective.server.resolver.MediaMetadata;
import com.alexk8s.spective.server.resolver.MediaType;
import dev.brachtendorf.jimagehash.hash.Hash;
import dev.brachtendorf.jimagehash.hashAlgorithms.PerceptiveHash;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import datadog.trace.api.Trace;
import reactor.netty.http.client.HttpClient;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class ContentService {

    private final WebClient fileWebClient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(HttpClient.create().followRedirect(true))).build();
    private final MediaMetadataRepository mediaMetadataRepository;
    private final String downloadPath;
    private final String preloadPath;
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static final String IMAGE_HASH_VERSION = "PerceptiveHash-64-v2";

    @Autowired
    public ContentService(MediaMetadataRepository mediaMetadataRepository, @Value("${CONTENT_DOWNLOAD_PATH:/tmp/spective_content}") String downloadPath,
                          @Value("${CONTENT_PRELOAD_PATH:/tmp/spective_preload}") String preloadPath) {

        this.mediaMetadataRepository = mediaMetadataRepository;
        this.downloadPath = downloadPath;
        this.preloadPath = preloadPath;
    }


    @PostConstruct
    public void init() {
        Path tmpPath = Path.of(downloadPath);
        if (!Files.exists(tmpPath)) {
            try {
                Files.createDirectory(tmpPath);
            } catch (IOException e) {
                log.error("Error creating local path for content service", e);
            }
        }
    }

    public Mono<DownloadMetadata> downloadContent(String url, String id) {
        String path = getPath(id);
        AtomicInteger statusCode = new AtomicInteger(-1);
        Flux<DataBuffer> dataBufferFlux = fileWebClient.get().uri(url).retrieve()
                .toEntityFlux(DataBuffer.class).checkpoint()
                .flatMapMany(r -> {
                    statusCode.set(r.getStatusCodeValue());
                    return r.getBody();
                })
                .onErrorComplete(e -> {
                    if (e instanceof WebClientResponseException) {
                        log.warn("Error loading response for {}", url, e);
                        statusCode.set(((WebClientResponseException) e).getRawStatusCode());
                        return true;
                    }
                    return false;
                });
        return dataBufferFlux.transform(d -> DataBufferUtils.write(d, Path.of(path), StandardOpenOption.CREATE)).checkpoint()
                .share().then(Mono.fromCallable(() -> new DownloadMetadata(path, statusCode.get())))
                ;
    }

    private record DownloadMetadata(String path, Integer statusCode) {
    }


    public String getPath(String id) {

        return downloadPath + "/" + id;
    }

    public Mono<MediaMetadata> applyContentMetadata(MediaMetadata mediaMetadata) {
        String id = UUID.randomUUID().toString();
        String originalUrl = mediaMetadata.getOriginalUrl();
        return mediaMetadataRepository.findByPostedUrlAndOriginalUrl(mediaMetadata.getPostedUrl(), originalUrl)
                .publishOn(Schedulers.boundedElastic())
                .flatMap(this::checkForRehash)
                .switchIfEmpty(
                        downloadContent(originalUrl, id).checkpoint().doOnError((e) -> log.error("Error loading content {}", originalUrl, e))
                                .map(d -> resolveMetadata(mediaMetadata, d))
                                .flatMap(mediaMetadataRepository::save)
                ).checkpoint()
                .map(this::updateUrlToPreloadedUrl);
    }

    private MediaMetadata updateUrlToPreloadedUrl(MediaMetadata mediaMetadata) {
        if (mediaMetadata.isPreloaded()) {
            String url = "https://server.spective.alexk8s.com/v1/content/preload/" + getResolvedHash(mediaMetadata);
            mediaMetadata.setFullUrl(url);
            if (mediaMetadata.getMediaType() == MediaType.IMAGE)
                mediaMetadata.setPreviewUrl(url + "?height=700");
            else
                mediaMetadata.setPreviewUrl(url);
        } else {
            mediaMetadata.setPreviewUrl(mediaMetadata.getOriginalUrl());
            mediaMetadata.setFullUrl(mediaMetadata.getOriginalUrl());
        }
        return mediaMetadata;
    }

    public Mono<MediaMetadata> checkForRehash(MediaMetadata mediaMetadata) {
        if (mediaMetadata.getMediaType() != MediaType.IMAGE || IMAGE_HASH_VERSION.equals(mediaMetadata.getImageHashVersion()) || mediaMetadata.getContentStatus()== MediaMetadata.ContentStatus.UNAVAILABLE)
            return Mono.just(mediaMetadata);
        log.info("Rehashing {}", mediaMetadata);
        if (mediaMetadata.getImageHash() != null) {
            Path currentPath = resolvePreloadPath(mediaMetadata.getImageHash());
            if (Files.exists(currentPath)) {
                String hash = hash(currentPath);
                if (hash.equals(mediaMetadata.getHash())) {
                    DownloadMetadata localDownloadMetadata = new DownloadMetadata(currentPath.toString(), mediaMetadata.getPreloadStatusCode());
                    return mediaMetadataRepository.save(resolveMetadata(mediaMetadata, localDownloadMetadata));
                }
                log.info("Hash of metadata didn't match hash of file Metadata: {} File: {}", mediaMetadata.getHash(), hash);
            }
        }
        String id = UUID.randomUUID().toString();
        String originalUrl = mediaMetadata.getOriginalUrl();
        return downloadContent(originalUrl, id).checkpoint().doOnError((e) -> log.error("Error loading content {}", originalUrl, e))
                .map(d -> resolveMetadata(mediaMetadata, d))
                .flatMap(mediaMetadataRepository::save);

    }


    @SneakyThrows
    private MediaMetadata resolveMetadata(MediaMetadata mediaMetadata, DownloadMetadata downloadMetadata) {
        Path path = Paths.get(downloadMetadata.path());
        mediaMetadata.setPreloadStatusCode(downloadMetadata.statusCode());
        if (mediaMetadata.getSize() == 0) mediaMetadata.setSize(Files.size(path));
        if (mediaMetadata.getSize() == 0) {
            log.warn("Downloaded file was empty {}", path);
            Files.delete(path);
            return mediaMetadata;
        }
        if (mediaMetadata.getMediaType() == MediaType.IMAGE) {
            mediaMetadata.setImageHash(hashImage(path));
            mediaMetadata.setImageHashVersion(IMAGE_HASH_VERSION);
            if (mediaMetadata.getHeight() == null) {
                BufferedImage bufferedImage = ImageIO.read(path.toFile());
                if (bufferedImage != null) {
                    mediaMetadata.setHeight(bufferedImage.getHeight());
                    mediaMetadata.setWidth(bufferedImage.getWidth());
                }
            }
        }
        mediaMetadata.setHash(hash(path));
        mediaMetadata.setHashTime(ZonedDateTime.now());
        try {
            moveToPreload(path, getResolvedHash(mediaMetadata));
            mediaMetadata.setPreloaded(true);
        } catch (IOException e) {
            log.warn("Error moving preload file {}", mediaMetadata, e);
        }
        return mediaMetadata;
    }

    private String getResolvedHash(MediaMetadata mediaMetadata) {

        if (mediaMetadata.getImageHash() != null && !mediaMetadata.getImageHash().equals("null"))
            return mediaMetadata.getImageHash();
        else
            return mediaMetadata.getHash();
    }

    private void moveToPreload(Path contentPath, String hash) throws IOException {
        Path preloadedPath = resolvePreloadPath(hash);
        if (!contentPath.equals(preloadedPath) && Files.exists(preloadedPath))
            Files.delete(contentPath);
        if (Files.notExists(preloadedPath.getParent()))
            Files.createDirectories(preloadedPath.getParent());
        Files.move(contentPath, preloadedPath);
    }

    private Path resolvePreloadPath(String hash) {

        return Paths.get(preloadPath, hash.substring(0, 1), hash.substring(0, 2), hash);
    }

    @SneakyThrows
    @Trace
    private String hashImage(Path path) {
        BufferedImage bufferedImage = ImageIO.read(path.toFile());
        if (bufferedImage == null) return "null";
        PerceptiveHash perceptiveHash = new PerceptiveHash(64);
        Hash hash = perceptiveHash.hash(bufferedImage);
        return toString(hash.toByteArray());
    }

    @SneakyThrows
    @Trace
    private String hash(Path path) {
        byte[] chunk = new byte[100 * 1024];
        int chunkLength;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        try (InputStream in = Files.newInputStream(path)) {
            while ((chunkLength = in.read(chunk)) != -1) {
                digest.update(chunk, 0, chunkLength);
            }
        }
        return toString(digest.digest());
    }

    public static String toString(byte[] hash) {
        char[] hexChars = new char[hash.length * 2];
        for (int j = 0; j < hash.length; j++) {
            int v = hash[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public String getLocalContentUrl(String id) {
        return "https://server.spective.alexk8s.com/v1/content/local/" + id;
    }

}
