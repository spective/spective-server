package com.alexk8s.spective.server.service;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.utility.FluxSubscriptionListener;
import com.eventstore.dbclient.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

@Slf4j
@AllArgsConstructor
@Service
public class TailService {

    private final EventStoreDBClient eventStoreDBClient;


    @SneakyThrows
    private RedditPostModel parseEvent(ResolvedEvent r) {
        return r.getEvent().getEventDataAs(RedditPostModel.class);
    }

    public Flux<RedditPostModel> liveStream() {
        log.info("Configuring live stream");
        FluxSubscriptionListener listener = new FluxSubscriptionListener();
        Flux<ResolvedEvent> resolvedEventFlux = Flux.create(listener::setSink);
        return Mono.fromFuture(eventStoreDBClient.subscribeToStream("reddit-posts", listener, SubscribeToStreamOptions.get().fromEnd()))
                .thenMany(resolvedEventFlux)
                .map(this::parseEvent);
    }

    public Flux<RedditPostModel> framedStream() {
        log.info("Configuring framed stream");
        Set<UUID> ids = new HashSet<>();
        ReadStreamOptions options = ReadStreamOptions.get().fromEnd().backwards();
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(2))
                .flatMap((i) -> eventStoreDBClient.readStreamReactive("reddit-posts",  options))
                .map(ReadMessage::getEvent)
                .filter(e -> ids.add(e.getEvent().getEventId()))
                .map(this::parseEvent)
                ;

    }

    public Flux<RedditPostModel> backwards(){
          ReadStreamOptions options = ReadStreamOptions.get().fromEnd().backwards();
        return Flux.from(eventStoreDBClient.readStreamReactive("reddit-posts", options))
                .map(ReadMessage::getEvent)
                .map(this::parseEvent);
    }



}
