package com.alexk8s.spective.server.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.client.Reddit4J;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;

@AllArgsConstructor
@Slf4j
public class Reddit4JFactory implements PooledObjectFactory<Reddit4J> {
    private final Reddit4JService reddit4JService;
    @Override
    public void activateObject(PooledObject<Reddit4J> p) throws Exception {
    }

    @Override
    public void destroyObject(PooledObject<Reddit4J> p) throws Exception {

    }

    @Override
    public PooledObject<Reddit4J> makeObject() throws Exception {
        log.info("Creating new client");
        return new DefaultPooledObject<>(reddit4JService.getReddit4J());
    }

    @Override
    public void passivateObject(PooledObject<Reddit4J> p) throws Exception {

    }

    @Override
    public boolean validateObject(PooledObject<Reddit4J> p) {
        return Duration.between(p.getCreateInstant(), Instant.now()).get(ChronoUnit.HOURS) < 4;
    }
}
