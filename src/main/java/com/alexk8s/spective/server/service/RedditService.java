package com.alexk8s.spective.server.service;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.client.Reddit4J;
import masecla.reddit4j.objects.RedditData;
import masecla.reddit4j.objects.RedditListing;
import masecla.reddit4j.objects.RedditPost;
import masecla.reddit4j.objects.Time;
import masecla.reddit4j.requests.SubredditPostListingEndpointRequest;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RedditService {

    private ObjectPool<Reddit4J> clients;
    private final Reddit4JService reddit4JService;


    @SneakyThrows
    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        clients = new GenericObjectPool<>(new Reddit4JFactory(reddit4JService));
    }

    @SneakyThrows
    public List<RedditPostModel> getRedditPosts(List<String> ids) {

        String name = ids.stream().map(id -> {
            if (id.startsWith("t3_"))
                return id;
            return "t3_" + id;
        }).collect(Collectors.joining(","));
        int retryCounter = 0;
        Reddit4J client = clients.borrowObject();
        try {
            while (true) {
                try {
                    Connection connection = client.useEndpoint("/api/info").data("id", name);
                    Connection.Response response = client.getHttpClient().execute(connection);
                    return parseResponse(response.body());
                } catch (SocketTimeoutException e) {
                    if (retryCounter++ > 3)
                        throw e;
                } catch (HttpStatusException e) {
                    if (e.getStatusCode() != 401 || retryCounter++ > 3)
                        throw e;
                    if (e.getStatusCode() == 401) {
                        clients.invalidateObject(client);
                        client = clients.borrowObject();
                    }
                }
            }
        } finally {
            clients.returnObject(client);
        }
    }

    @SneakyThrows
    public List<RedditPostModel> getHotPosts(String subreddit,String after) {
        Reddit4J reddit4J = clients.borrowObject();
        try {
            SubredditPostListingEndpointRequest request = reddit4J.getSubreddit(subreddit).getHot().time(Time.WEEK);
            if(after!=null)
                request.after(after);
            List<RedditPost> submit = request.submit();
            return submit.stream().map(this::mapRedditPost).toList();
        } finally {
            clients.returnObject(reddit4J);
        }
    }

    private RedditPostModel mapRedditPost(RedditPost redditPost) {
        RedditPostModel redditPostModel = new RedditPostModel();
        BeanUtils.copyProperties(redditPost, redditPostModel);
        return redditPostModel;
    }

    @SneakyThrows
    private List<RedditPostModel> parseResponse(String body) {
        RedditData<RedditListing<RedditData<RedditPostModel>>> fromJson = objectMapper.readValue(body, new TypeReference<>() {
        });

        return fromJson.data().children().stream().map(RedditData::data).toList();
    }

    private ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    private record RedditData<T>(String kind, T data) {
    }

    private record RedditListing<T>(List<T> children) {
    }


}
