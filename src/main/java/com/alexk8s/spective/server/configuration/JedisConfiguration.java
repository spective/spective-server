package com.alexk8s.spective.server.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPooled;

@Configuration
public class JedisConfiguration {

    @Bean
    public JedisPooled jedisPooled(@Value("${REDIS_HOST:localhost}")String redisHost){
        return new JedisPooled(redisHost,6379);
    }
}
