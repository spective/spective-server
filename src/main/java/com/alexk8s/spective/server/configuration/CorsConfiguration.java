package com.alexk8s.spective.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
public class CorsConfiguration implements WebFluxConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        corsRegistry.addMapping("/v1/**")
                .allowedOrigins("http://localhost:5173","http://localhost:4173", "https://app.spective.alexk8s.com")
                .allowedMethods("PUT", "GET", "POST", "OPTIONS","DELETE")
                .maxAge(3600);
    }
}
