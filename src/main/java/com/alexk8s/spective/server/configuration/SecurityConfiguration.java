package com.alexk8s.spective.server.configuration;

import com.alexk8s.spective.server.authentication.AuthenticationManager;
import com.alexk8s.spective.server.authentication.SecurityContextRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfiguration {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http){
        http.csrf().disable();
        http.httpBasic().disable();
        return http
                .authenticationManager(new AuthenticationManager())
                .securityContextRepository(new SecurityContextRepository())
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS,"/v1/**").permitAll()
                .pathMatchers(HttpMethod.GET,"/v1/gif-service-content/*","/v1/content/**","/health","/v1/post/**","/v1/search","/v1/hot").permitAll()
                .anyExchange().authenticated().and()
                .build();
    }


}
