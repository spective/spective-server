package com.alexk8s.spective.server.configuration;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.cassandra.core.convert.CassandraConverter;
import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;

import java.time.ZonedDateTime;
import java.util.Arrays;

@AllArgsConstructor
@Configuration
public class CassandraMappingConfiguration {


        @Bean
        CustomConversions customConversions() {
            return new CustomConversions(CustomConversions.StoreConversions.NONE,Arrays.asList(
                    StringToZonedDateTimeConverter.INSTANCE, ZonedDateTimeToStringConverter.INSTANCE,
                    StringToMediaMetadataConverter.INSTANCE,MediaMetadataToStringConverter.INSTANCE,
                    StringToRedditMediaConverter.INSTANCE,RedditMediaToStringConverter.INSTANCE
                    ));
        }


        @Bean
        public CassandraConverter cassandraConverter(CassandraMappingContext mapping, CustomConversions customConversions) {

            MappingCassandraConverter mappingCassandraConverter = new MappingCassandraConverter(mapping);
            mappingCassandraConverter.setCustomConversions(customConversions);

            return mappingCassandraConverter;
        }

        @ReadingConverter
         enum StringToZonedDateTimeConverter implements Converter<String, ZonedDateTime> {
            INSTANCE;

            @Override
            public ZonedDateTime convert(String source) {
                return ZonedDateTime.parse(source);
            }
        }

        @WritingConverter
         enum ZonedDateTimeToStringConverter implements Converter<ZonedDateTime, String> {
            INSTANCE;


            @Override
            public String convert(ZonedDateTime source) {
                return source.toString();
            }
        }

        private static final ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        @ReadingConverter
    enum StringToMediaMetadataConverter implements Converter<String, RedditPostModel.RedditMediaMetadataMap>{
            INSTANCE;

            @Override
            @SneakyThrows
            public RedditPostModel.RedditMediaMetadataMap convert(String source) {
                return objectMapper.readValue(source, RedditPostModel.RedditMediaMetadataMap.class);
            }
        }

    enum MediaMetadataToStringConverter implements Converter<RedditPostModel.RedditMediaMetadataMap,String>{
        INSTANCE;

        @Override
        @SneakyThrows
        public String convert(RedditPostModel.RedditMediaMetadataMap source) {
            return objectMapper.writeValueAsString(source);
        }
    }
    @ReadingConverter
    enum StringToRedditMediaConverter implements Converter<String, RedditPostModel.RedditMedia>{
        INSTANCE;

        @Override
        @SneakyThrows
        public RedditPostModel.RedditMedia convert(String source) {
            return objectMapper.readValue(source, RedditPostModel.RedditMedia.class);
        }
    }

    enum RedditMediaToStringConverter implements Converter<RedditPostModel.RedditMedia,String>{
        INSTANCE;

        @Override
        @SneakyThrows
        public String convert(RedditPostModel.RedditMedia source) {
            return objectMapper.writeValueAsString(source);
        }
    }
}

