package com.alexk8s.spective.server.configuration;

import com.eventstore.dbclient.EventStoreDBClient;
import com.eventstore.dbclient.EventStoreDBClientSettings;
import com.eventstore.dbclient.EventStoreDBConnectionString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
public class EventStoreDBConfiguration {

    @Bean
    public EventStoreDBClientSettings eventStoreDBClientSettings(@Value("${EVENTSTORE_HOST:localhost:2113}")String eventstoreHost){
        return EventStoreDBConnectionString.parseOrThrow("esdb://"+eventstoreHost+"?tls=false");
    }

    @Bean
    public EventStoreDBClient eventStoreDBClient(EventStoreDBClientSettings settings){
        return EventStoreDBClient.create(settings);
    }


}
