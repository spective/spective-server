package com.alexk8s.spective.server.utility;

import io.appwrite.Client;

public class AppwriteUtility {

    public static final String APPWRITE_ENDPOINT = "https://appwrite.alexk8s.com/v1";
    public static final String APPWRITE_PROJECT = "6362f2c943b93e6c0bcd";
    public static Client getClient(String token){
        return new Client(APPWRITE_ENDPOINT).setProject(APPWRITE_PROJECT).setJWT(token);
    }
}
