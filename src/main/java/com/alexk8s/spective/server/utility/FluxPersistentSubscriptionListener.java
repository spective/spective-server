package com.alexk8s.spective.server.utility;

import com.eventstore.dbclient.*;
import lombok.AllArgsConstructor;
import lombok.Setter;
import reactor.core.publisher.FluxSink;

@AllArgsConstructor
public class FluxPersistentSubscriptionListener extends PersistentSubscriptionListener {
    private final FluxSink<ResolvedEvent> sink;

    @Override
    public void onEvent(PersistentSubscription subscription, int retryCount, ResolvedEvent event) {
        sink.next(event);
    }

    @Override
    public void onError(PersistentSubscription subscription, Throwable throwable) {
        sink.error(throwable);
    }

    @Override
    public void onCancelled(PersistentSubscription subscription) {
        sink.complete();
    }
}
