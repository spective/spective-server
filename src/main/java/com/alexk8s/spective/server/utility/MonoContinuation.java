package com.alexk8s.spective.server.utility;

import kotlin.Result;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.MonoSink;

@Slf4j
public class MonoContinuation<T> implements Continuation<T> {
    @Setter
    private MonoSink<T> monoSink;

    @NotNull
    @Override
    public CoroutineContext getContext() {
        return EmptyCoroutineContext.INSTANCE;
    }

    @Override
    public void resumeWith(@NotNull Object o) {
        if(o instanceof Result.Failure){
            log.warn("Appwrite error {}",((Result.Failure) o).exception.getMessage());
            monoSink.error(((Result.Failure) o).exception);
        } else{
            monoSink.success((T)o);
        }
    }
}
