package com.alexk8s.spective.server.utility;

import com.eventstore.dbclient.ResolvedEvent;
import com.eventstore.dbclient.Subscription;
import com.eventstore.dbclient.SubscriptionListener;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import reactor.core.publisher.FluxSink;

public class FluxSubscriptionListener extends SubscriptionListener {
    @Setter
    private  FluxSink<ResolvedEvent> sink;

    @Override
    public void onEvent(Subscription subscription, ResolvedEvent event) {
        sink.next(event);
    }

    @Override
    public void onError(Subscription subscription, Throwable throwable) {
        sink.error(throwable);
    }

    @Override
    public void onCancelled(Subscription subscription) {
        sink.complete();
    }
}
