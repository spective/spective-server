package com.alexk8s.spective.server.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Checkpoint {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String process;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED,value = "checkpoint_time")
    private ZonedDateTime checkpointTime;
    private long value;
}
