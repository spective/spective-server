package com.alexk8s.spective.server.model;

import com.alexk8s.spective.server.resolver.MediaMetadata;
import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Table("reddit_post")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RedditPostModel {

    @PrimaryKey
    private String id;

    private String name;

    /**
     * the account name of the poster. null if this is a promotional link
     */
    private String author;

    /**
     * the CSS class of the author's flair. subreddit specific
     */
    @JsonProperty("author_flair_css_class")
    @Column("author_flair_css_class")
    private String authorFlairCssClass;

    /**
     * the text of the author's flair. subreddit specific
     */
    @JsonProperty("author_flair_text")
    @Column("author_flair_text")
    private String authorFlairText;

    /**
     * true if the current user has already clicked on this link.
     */
    private boolean clicked;

    /**
     * the domain of this link. Self posts will be self.<subreddit> while other examples include en.wikipedia.org and s3.amazon.com
     */
    private String domain;

    /**
     * true if the post is hidden by the logged in user. false if not logged in or not hidden.
     */
    private boolean hidden;

    /**
     * true if this link is a selfpost
     */
    @JsonProperty("is_self")
    private boolean is_self;

    /**
     * how the logged-in user has voted on the link - True = upvoted, False = downvoted, null = no vote
     */
    private Boolean likes;

    /**
     * the CSS class of the link's flair.
     */
    @JsonProperty("link_flair_css_class")
    @Column("link_flair_css_class")
    private String linkFlairCssClass;

    /**
     * the text of the link's flair.
     */
    @JsonProperty("link_flair_text")
    @Column("link_flair_text")
    private String linkFlairText;

    /**
     * whether the link is locked (closed to new comments) or not.
     */
    private boolean locked;

    /**
     * Used for streaming video. Detailed information about the video and it's origins are placed here
     */
    private RedditMedia media;

    /**
     * Used for streaming video. Technical embed specific information is found here.
     */
    @JsonProperty("media_embed")
    @Transient
    private Object mediaEmbed;

    /**
     * the number of comments that belong to this link. includes removed comments.
     */
    @JsonProperty("num_comments")
    @Column("num_comments")
    private int numComments;

    /**
     * true if the post is tagged as NSFW. False if otherwise
     */
    @JsonProperty("over_18")
    @Column("over_18")
    private boolean over18;

    /**
     * relative URL of the permanent link for this link
     */
    private String permalink;

    /**
     * true if this post is saved by the logged in user
     */
    private boolean saved;

    /**
     * the net-score of the link. note: A submission's score is simply the number of upvotes minus the number of downvotes. If five users like the submission and three users don't it will have a score of 2. Please note that the vote numbers are not "real" numbers, they have been "fuzzed" to prevent spam bots etc. So taking the above example, if five users upvoted the submission, and three users downvote it, the upvote/downvote numbers may say 23 upvotes and 21 downvotes, or 12 upvotes, and 10 downvotes. The points score is correct, but the vote totals are "fuzzed".
     */
    private int score;

    /**
     * the raw text. this is the unformatted text which includes the raw markup characters such as ** for bold. <, >, and & are escaped. Empty if not present.
     */
    private String selftext;

    /**
     * the formatted escaped HTML text. this is the HTML formatted version of the marked up text. Items that are boldened by ** or *** will now have <em> or *** tags on them. Additionally, bullets and numbered lists will now be in HTML list format. NOTE: The HTML string will be escaped. You must unescape to get the raw HTML. Null if not present.
     */
    @JsonProperty("selftext_html")
    @Column("selftext_html")
    private String selftextHtml;

    /**
     * subreddit of thing excluding the /r/ prefix. "pics"
     */
    private String subreddit;

    /**
     * the id of the subreddit in which the thing is located
     */
    @JsonProperty("subreddit_id")
    @Column("subreddit_id")
    private String subredditId;

    /**
     * full URL to the thumbnail for this link; "self" if this is a self post; "image" if this is a link to an image but has no thumbnail; "default" if a thumbnail is not available
     */
    private String thumbnail;

    /**
     * the title of the link. may contain newlines for some reason
     */
    private String title;

    /**
     * the link of this post. the permalink if this is a self-post
     */
    private String url;

    /**
     * Indicates if link has been edited. Will be the edit timestamp if the link has been edited and return false otherwise. https://github.com/reddit/reddit/issues/581
     * TODO: The API can return a long or a boolean, needs custom deserialization to handle it
     */
    //private long edited;

    /**
     * to allow determining whether they have been distinguished by moderators/admins. null = not distinguished. moderator = the green [M]. admin = the red [A]. special = various other special distinguishes http://bit.ly/ZYI47B
     */
    private String distinguished;

    /**
     * true if the post is set as the sticky in its subreddit.
     */
    private boolean stickied;

    @JsonProperty("author_fullname")
    @Column("author_fullname")
    private String authorFullname;

    @Column("mod_reason_title")
    @JsonProperty("mod_reason_title")
    private String modReasonTitle;
    private int gilded;
    @Transient
    @JsonAlias("link_flair_richtext")
    private List<Object> linkFlairRichtext;
    @Column("subreddit_name_prefixed")
    @JsonAlias("subreddit_name_prefixed")
    private String subredditNamePrefixed;
    private int pwls;

    /**
     * The number of upvotes. (includes own).
     * <p>
     * https://github.com/reddit-archive/reddit/wiki/JSON#votable-implementation
     */
    private int ups;

    /**
     * The number of downvotes. (includes own).
     * <p>
     * https://github.com/reddit-archive/reddit/wiki/JSON#votable-implementation
     */
    private int downs;

    /**
     * true if thing is liked by the user, false if thing is disliked, null if the user has not voted or you are not logged in.
     * Certain languages such as Java may need to use a boolean wrapper that supports null assignment.
     * <p>
     * https://github.com/reddit-archive/reddit/wiki/JSON#votable-implementation
     */
    private Boolean liked;

    /**
     * The time of creation in local epoch-second format. ex: 1331042771.0
     * <p>
     * https://github.com/reddit-archive/reddit/wiki/JSON#created-implementation
     */
    private long created;

    /**
     * The time of creation in UTC epoch-second format. Note that neither of these ever have a non-zero fraction.
     * <p>
     * https://github.com/reddit-archive/reddit/wiki/JSON#created-implementation
     */
    @JsonProperty("created_utc")
    @Column("created_utc")
    private long createdUtc;
    @Transient
    private List<MediaMetadata> mediaMetadata;

    @Transient
    public MediaMetadata.ContentStatus getContentStatus() {
        if (mediaMetadata == null || mediaMetadata.isEmpty())
            return null;
        List<MediaMetadata.ContentStatus> contentStatuses = mediaMetadata.stream().map(MediaMetadata::getContentStatus).distinct().toList();
        if (contentStatuses.size() > 1)
            return MediaMetadata.ContentStatus.MIXED;
        return contentStatuses.get(0);
    }


    @JsonAlias("approved_at_utc")
    @Column("approved_at_utc")
    private Long approvedAtUTC;
    @JsonAlias("is_gallery")
    @Column("is_gallery")
    private Boolean isGallery;
    @JsonAlias("thumbnail_height")
    @Column("thumbnail_height")
    private Integer thumbnailHeight;
    @JsonAlias("thumbnail_width")
    @Column("thumbnail_width")
    private Integer thumbnailWidth;

    @Column("media_metadata")
    @JsonAlias("media_metadata")
    private RedditMediaMetadataMap redditMediaMetadata;


    public static class RedditMediaMetadataMap extends HashMap<String, RedditMediaMetadata> {
    }

    public record RedditMediaMetadata(String status, @JsonAlias("e") String type, @JsonAlias("m") String mimeType,
                                      @JsonAlias("p") List<RedditPreview> previews,
                                      @JsonAlias("s") RedditPreview source, String id) {

    }

    public record RedditPreview(@JsonAlias("y") int height, @JsonAlias("x") int width, @JsonAlias("u") String url) {
    }


    public record RedditMedia(@JsonAlias("reddit_video") RedditVideo redditVideo, String type, RedditOEmbed oembed) {
    }

    public record RedditVideo(@JsonAlias("bitrate_kbps") int bitrateKbps, @JsonAlias("fallback_url") String fallbackUrl,
                              int height, int width,
                              @JsonAlias({"scrubber_media_url", "srubberMediaUrl"}) String scrubberMediaUrl,
                              int duration, @JsonAlias("is_gif") Boolean isGif) {
    }

    public record RedditOEmbed(@JsonAlias("provider_url") String providerUrl, String version, String type, String title,
                               @JsonAlias("thumbnail_width") int thumbnailWidth, int height, int width, String html,
                               @JsonAlias("author_name") String authorName,
                               @JsonAlias("provider_nam") String providerName,
                               @JsonAlias("thumbnail_url") String thumbnailUrl,
                               @JsonAlias("thumbnail_height") int thumbnailHeight,
                               @JsonAlias("author_url") String authorUrl, String description,
                               @JsonAnySetter @JsonAnyGetter Map<String, Object> additionalFields) {

    }
}
