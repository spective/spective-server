package com.alexk8s.spective.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table("reddit_post_id")
@AllArgsConstructor
@NoArgsConstructor
public class RedditPostId {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String type;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String value;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED,name = "created_utc")
    private Long createdUtc;
    private String id;
}
