package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import masecla.reddit4j.objects.RedditPost;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import static com.alexk8s.spective.server.resolver.MediaType.*;

public interface Resolver {
    ResolverType getType();

    Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost);

    default String resolveOriginId(String url) {
        String[] split = url.split("/");
        return split[split.length - 1].split("[.?]")[0];
    }

    default MediaType resolveMediaType(String url) {
        int queryIndex = url.indexOf("?");
        int end = queryIndex == -1 ? url.length() : queryIndex;
        String path = url.substring(url.indexOf("/"), end);
        if (path.indexOf('.') == -1 || path.lastIndexOf('.')==path.length()-1)
            return UNKNOWN;
        String fileSuffix = path.substring(path.lastIndexOf('.')+1);
        if (IMAGE.getFileTypes().contains(fileSuffix))
            return IMAGE;
        if (GIF.getFileTypes().contains(fileSuffix))
            return GIF;
        if (VIDEO.getFileTypes().contains(fileSuffix))
            return VIDEO;
        return UNKNOWN;
    }

}

