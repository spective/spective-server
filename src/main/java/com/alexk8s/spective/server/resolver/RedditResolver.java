package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.service.ContentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Slf4j
public class RedditResolver implements Resolver {

    @Override
    public ResolverType getType() {
        return ResolverType.REDDIT;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        if (redditPost.getRedditMediaMetadata() == null) {
            return Mono.empty();
        }
        Stream<MediaMetadata> metadataStream = redditPost.getRedditMediaMetadata().values()
                .stream()
                .map(m -> convert(m, redditPost))
                .filter(Objects::nonNull);
        return Flux.fromStream(metadataStream);
    }

    private MediaMetadata convert(RedditPostModel.RedditMediaMetadata source, RedditPostModel redditPost) {
        MediaMetadata.MediaMetadataBuilder builder = MediaMetadata.builder()
                .postedUrl(redditPost.getUrl())
                .domain(redditPost.getDomain())
                .nsfw(redditPost.isOver18())
                .resolverType(ResolverType.REDDIT);
        if (source.source()!=null && source.source().url() != null) {
            String url = source.source().url().replaceAll("&amp;", "&");
            return builder.originalUrl(url)
                    .mediaType(resolveMediaType(url))
                    .width(source.source().width())
                    .height(source.source().height())
                    .build();
        } else if(source.previews()!=null){
            Comparator<RedditPostModel.RedditPreview> previewComparator = Comparator.comparing(p -> p.width() * p.height());
            Optional<RedditPostModel.RedditPreview> first = source.previews().stream().filter(p -> p.url() != null)
                    .sorted(previewComparator.reversed())
                    .findFirst();
            if (first.isPresent()) {

                RedditPostModel.RedditPreview redditPreview = first.get();
                String url = redditPreview.url().replaceAll("&amp;", "&");
                return builder.originalUrl(url)
                        .mediaType(resolveMediaType(url))
                        .width(redditPreview.width())
                        .height(redditPreview.height())
                        .build();
            } else {
                log.warn("null source for media metadata {} {}", redditPost.getId(), source);
                return builder.build();
            }
        }
        return  null;
    }

}
