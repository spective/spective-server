package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.service.ContentService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class GifServiceResolver implements Resolver {
    private final WebClient webClient;
    private final ContentService contentService;

    private final Mono<String> apiTokenMono;

    public GifServiceResolver(String apiUrl, ContentService contentService) {
        webClient = WebClient.builder().baseUrl(apiUrl).build();
        this.contentService = contentService;
        apiTokenMono = getApiToken().cache(Duration.ofMinutes(5));
    }

    private Mono<String> getApiToken() {
        return webClient.get().uri("/v2/auth/temporary")
                .retrieve()
                .bodyToMono(TemporaryApiTokenResponse.class)
                .map(TemporaryApiTokenResponse::token)
                .doOnNext(t -> {
                    log.info("Getting new api token {}", t);
                });
    }

    private record TemporaryApiTokenResponse(String token, String addr, String agent, String rtfm) {
    }

    @Override
    public ResolverType getType() {
        return ResolverType.GIF_SERVICE;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        String id = resolveOriginId(redditPost.getUrl());
        return apiTokenMono.flatMap(token -> callApi(token, id))
                .flatMap(x -> downloadHd(x, id))
                .map(w->
                        MediaMetadata.builder()
                                .postedUrl(redditPost.getUrl())
                                .originalUrl(contentService.getLocalContentUrl(id+".mp4"))
                                .mediaType(MediaType.VIDEO)
                                .resolverType(ResolverType.GIF_SERVICE)
                                .nsfw(redditPost.isOver18())
                                .domain(redditPost.getDomain())
                                .width(w.gif().width())
                                .height(w.gif().height())
                                .build()
                )
                .onErrorComplete(WebClientResponseException.Gone.class)
                .onErrorComplete(WebClientResponseException.NotFound.class)
                .onErrorComplete(WebClientResponseException.Forbidden.class)
                ;
    }

    private Mono<GifServiceWrapper> callApi(String token, String id) {
        return webClient.get().uri("/v2/gifs/{id}", Map.of("id", id))
                .header("Authorization", "Bearer " + token)
                .retrieve()
                .bodyToMono(GifServiceWrapper.class);
    }

    @SneakyThrows
    private Mono<GifServiceWrapper> downloadHd(GifServiceWrapper wrapper, String id) {
        String hdUrl = wrapper.gif().urls().hd();
        return contentService.downloadContent(hdUrl,id+".mp4").checkpoint().thenReturn(wrapper);
    }

    private record GifServiceWrapper(GifServiceResponse gif) {
    }

    private record GifServiceResponse(String id,
                                      Long createdDate,
                                      Boolean hasAudio,
                                      Integer width,
                                      Integer height,
                                      Long likes,
                                      List<String> tags,
                                      Boolean verified,
                                      Long views,
                                      Double duration,
                                      Boolean published,
                                      Integer type,
                                      GifServiceUrls urls,
                                      String userName,
                                      String avgColor,
                                      Object gallery,
                                      GifServiceUser user
    ) {
    }

    private record GifServiceUrls(String sd, String hd, String gif, String poster, String thumbnail,
                                  String vthumbnail) {
    }

    private record GifServiceUser(Long creationtime, Long followers, Long following, Integer gifs, String name,
                                  String profileImageUrl, String profileUrl, Integer publishedGifs,
                                  Integer subscription,
                                  String url, String username, Boolean verified, Long views) {
    }
}
