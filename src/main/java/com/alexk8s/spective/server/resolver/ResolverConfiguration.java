package com.alexk8s.spective.server.resolver;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.ZonedDateTime;

@Data
@Table("resolver_configuration")
public class ResolverConfiguration {
    @PrimaryKey
    private String domain;
    @Column("api_key")
    private String apiKey;
    @Column("resolver_type")
    private ResolverType resolverType;
    @Column("created_time")
    private ZonedDateTime createdTime;
    @Column("created_by")
    private String createdBy;
    private String pattern;

}
