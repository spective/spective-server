package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

public class RewritePatternResolver implements Resolver{

    private final static String ID_REPLACEABLE="{id}";
    private final String pattern;

    public RewritePatternResolver(String pattern) {
        this.pattern = pattern;
        if(!pattern.contains(ID_REPLACEABLE)){
            throw new RuntimeException("Nothing in pattern to replace");
        }
    }

    @Override
    public ResolverType getType() {
        return ResolverType.REWRITE;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        String id = resolveOriginId(redditPost.getUrl());
        String url = pattern.replace(ID_REPLACEABLE, id);
        MediaMetadata mediaMetadata = MediaMetadata.builder()
                .postedUrl(redditPost.getUrl())
                .originalUrl(url)
                .resolverType(ResolverType.REWRITE)
                .nsfw(redditPost.isOver18())
                .domain(redditPost.getDomain())
                .mediaType(resolveMediaType(url))
                .build();
        return Mono.just(mediaMetadata);
    }
}
