package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import masecla.reddit4j.objects.RedditPost;
import reactor.core.publisher.Flux;

public class NoopResolver implements Resolver{
    @Override
    public ResolverType getType() {
        return null;
    }

    @Override
    public Flux<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        return Flux.empty();
    }
}
