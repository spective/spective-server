package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import masecla.reddit4j.objects.RedditPost;
import org.reactivestreams.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.List;

public class ImgurResolver implements Resolver{
    private final String apiKey;
    private final WebClient webClient;

    public ImgurResolver(String apiKey) {
        this.apiKey = apiKey;
        this.webClient = WebClient.builder().baseUrl("https://api.imgur.com/3/").build();
    }

    @Override
    public ResolverType getType() {
        return ResolverType.IMGUR;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        if (redditPost.getUrl().contains("/a/")) {
            return resolveAlbumContent(redditPost);
        }
        return resolveImageContent(redditPost);
    }

    Mono<MediaMetadata> resolveImageContent(RedditPostModel redditPost) {
        String id = resolveOriginId(redditPost.getUrl());
        return webClient.get()
                .uri("image/{id}", id)
                .header("Authorization", "Client-ID " + apiKey)
                .retrieve()
                .bodyToMono(ImgurImageWrapper.class)
                .onErrorResume(WebClientResponseException.class, ex -> ex.getStatusCode() == HttpStatus.NOT_FOUND ? Mono.empty() : Mono.error(ex))
                .map(ImgurImageWrapper::data)
                .map(i -> parseImage(i, redditPost));

    }

    Flux<MediaMetadata> resolveAlbumContent(RedditPostModel redditPost) {
        String id = resolveOriginId(redditPost.getUrl()).split("#")[0];
        return webClient.get()
                .uri("album/{id}/images", id)
                .header("Authorization", "Client-ID " + apiKey)
                .retrieve()
                .bodyToMono(ImgurImageListWrapper.class)
                .onErrorResume(WebClientResponseException.class, ex -> ex.getStatusCode() == HttpStatus.NOT_FOUND ? Mono.empty() : Mono.error(ex))
                .flatMapIterable(ImgurImageListWrapper::data)
                .map(i -> parseImage(i, redditPost));

    }

    MediaMetadata parseImage(ImgurImage imgurImage, RedditPostModel redditPost) {
        return MediaMetadata.builder()
                .postedUrl(redditPost.getUrl())
                .originalUrl(imgurImage.link())
                .domain(redditPost.getDomain())
                .size(imgurImage.size())
                .nsfw(imgurImage.nsfw() || redditPost.isOver18())
                .resolverType(ResolverType.IMGUR)
                .mediaType(resolveMediaType(imgurImage.link()))
                .width(imgurImage.width())
                .height(imgurImage.height())
                .build();
    }



    private record ImgurImage(String id, String title, String description, Date datetime, boolean animated,
                              int width, int height, long size, long views, long bandwidth, String link, String gifv,
                              String mp4, @JsonProperty("mp4_size")String mp4Size,boolean nsfw){}

    private record ImgurImageWrapper (ImgurImage data){}
    private record ImgurImageListWrapper(List<ImgurImage> data){}
}
