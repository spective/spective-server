package com.alexk8s.spective.server.resolver;

public enum ResolverType {
    IMGUR,SIMPLE,REWRITE,GIF_SERVICE,REDDIT,V_REDDIT
}
