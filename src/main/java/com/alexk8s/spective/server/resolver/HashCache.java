package com.alexk8s.spective.server.resolver;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.Instant;
import java.time.ZonedDateTime;

@Table("hash_cache")
@Data
public class HashCache {
    @PrimaryKey
    private String url;
    private String hash;
    @Column("hash_time")
    private ZonedDateTime hashTime;
    @Column("image_hash")
    private String imageHash;
    @Column("origin_id")
    private String originId;
    private Boolean preloaded;
    @Column("reddit_id")
    private String redditId;
    private long size;
    private String subreddit;
}
