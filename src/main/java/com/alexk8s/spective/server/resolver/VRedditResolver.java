package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

public class VRedditResolver implements Resolver{
    @Override
    public ResolverType getType() {
        return ResolverType.V_REDDIT;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        if(redditPost.getMedia()==null || redditPost.getMedia().redditVideo()==null)
            return Mono.empty();
        RedditPostModel.RedditVideo redditVideo = redditPost.getMedia().redditVideo();
        if(redditVideo.fallbackUrl()==null)
            return Mono.empty();
        MediaMetadata build = MediaMetadata.builder()
                .postedUrl(redditPost.getUrl())
                .domain(redditPost.getDomain())
                .nsfw(redditPost.isOver18())
                .originalUrl(redditVideo.fallbackUrl())
                .mediaType(resolveMediaType(redditVideo.fallbackUrl()))
                .width(redditVideo.width())
                .height(redditVideo.height())
                .resolverType(ResolverType.V_REDDIT).build();
        return Mono.just(build);
    }
}
