package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.model.RedditPostModel;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

public class SimpleResolver implements Resolver {
    @Override
    public ResolverType getType() {
        return ResolverType.SIMPLE;
    }

    @Override
    public Publisher<MediaMetadata> resolveMetadata(RedditPostModel redditPost) {
        MediaMetadata mediaMetadata = MediaMetadata.builder()
                .postedUrl(redditPost.getUrl())
                .originalUrl(redditPost.getUrl())
                .domain(redditPost.getDomain())
                .nsfw(redditPost.isOver18())
                .resolverType(ResolverType.SIMPLE)
                .mediaType(resolveMediaType(redditPost.getUrl()))
                .build();
        return Mono.just(mediaMetadata);
    }
}
