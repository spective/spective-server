package com.alexk8s.spective.server.resolver;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collections;
import java.util.Set;

@Getter
@AllArgsConstructor
public enum MediaType {
    IMAGE(Set.of("jpg", "jpeg", "png")),
    GIF(Set.of("gif")),
    VIDEO(Set.of("mp4", "webm", "gifv")),
    UNKNOWN(Collections.emptySet());

    private final Set<String> fileTypes;
}
