package com.alexk8s.spective.server.resolver;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
@Table("media_metadata")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MediaMetadata {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, name = "posted_url")
    private String postedUrl;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, name = "original_url")
    private String originalUrl;
    private String domain;
    private String hash;
    @Column("image_hash")
    private String imageHash;
    private long size;
    private boolean preloaded;
    @Column("hash_time")
    private ZonedDateTime hashTime;
    private boolean nsfw;
    @Column("resolver_type")
    private ResolverType resolverType;
    @Column("media_type")
    private MediaType mediaType;
    private Integer width;
    private Integer height;
    @Transient
    private String previewUrl;
    @Transient
    private String fullUrl;
    @Column("preload_status_code")
    private Integer preloadStatusCode;
    @Column("hash_version")
    private String imageHashVersion;

    public enum ContentStatus {
        OK, UNAVAILABLE, UNKNOWN,MIXED;
    }

    @Transient
    public ContentStatus getContentStatus() {
        if (preloadStatusCode != null && preloadStatusCode == 404)
            return ContentStatus.UNAVAILABLE;
        if (!preloaded || preloadStatusCode == null)
            return ContentStatus.UNKNOWN;
        if (preloadStatusCode > 399)
            return ContentStatus.UNAVAILABLE;
        return ContentStatus.OK;
    }

}
