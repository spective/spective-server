package com.alexk8s.spective.server.resolver;

import com.alexk8s.spective.server.repository.ResolverConfigurationRepository;
import com.alexk8s.spective.server.service.ContentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
@Slf4j
public class ResolverFactory {
    private final ResolverConfigurationRepository repository;
    private final ContentService contentService;

    public Mono<Resolver> getResolver(String domain) {
        return repository.findById(domain)
                .map(this::buildResolver);
    }

    public Resolver buildResolver(ResolverConfiguration resolverConfiguration) {
        return switch (resolverConfiguration.getResolverType()) {
            case IMGUR -> new ImgurResolver(resolverConfiguration.getApiKey());
            case SIMPLE -> new SimpleResolver();
            case REWRITE -> new RewritePatternResolver(resolverConfiguration.getPattern());
            case GIF_SERVICE -> new GifServiceResolver(resolverConfiguration.getApiKey(),contentService);
            case REDDIT -> new RedditResolver();
            case V_REDDIT -> new VRedditResolver();
            default -> new NoopResolver();
        };
    }
}
