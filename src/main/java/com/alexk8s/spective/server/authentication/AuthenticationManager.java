package com.alexk8s.spective.server.authentication;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

public class AuthenticationManager implements ReactiveAuthenticationManager {
    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        if(authentication instanceof BearerAuthenticationToken){
            authentication.setAuthenticated(true);
        }
        return Mono.just(authentication);
    }
}
