package com.alexk8s.spective.server.authentication;

import com.alexk8s.spective.server.utility.MonoContinuation;
import io.appwrite.Client;
import io.appwrite.models.Account;
import io.appwrite.models.TeamList;
import io.appwrite.services.Teams;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

import static com.alexk8s.spective.server.utility.AppwriteUtility.getClient;

@Slf4j
public class SecurityContextRepository implements ServerSecurityContextRepository {


    private static final String ADMIN_TEAM_ID = "636309f1149d00f52166";
    @Override
    public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
        return null;
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange exchange) {
        String authorization = exchange.getRequest().getHeaders().getFirst("Authorization");
        if (authorization != null && authorization.startsWith("Bearer ")) {
            String token = authorization.substring(7);
            Client client = getClient(token);
            return Mono.zip(getAccount(client), isAdmin(client))
                    .map(tuple -> {
                        Account account = tuple.getT1();
                        Boolean isAdmin = tuple.getT2();
                        List<SimpleGrantedAuthority> authorities = (isAdmin ? List.of("USER","ADMIN") : List.of("USER")).stream().map(SimpleGrantedAuthority::new).toList();
                        return new SecurityContextImpl(new BearerAuthenticationToken(account.getId(), account.getName(), account.getEmail(), authorities, true));
                    })
                    .cache() // it looks like this mono is subscribed to multiple times. To prevent excess calls to appwrite the response is cached
                    .cast(SecurityContext.class)
                    .onErrorReturn(e -> e.getMessage().contains("Failed to verify JWT"), getAnonymousContext(token))
                    ;
        }
        return Mono.empty();
    }


    private SecurityContext getAnonymousContext(String token) {
        return new SecurityContextImpl(new AnonymousAuthenticationToken("unknown", token, List.of(new SimpleGrantedAuthority("ANONYMOUS"))));
    }

    @SneakyThrows
    private Mono<Account> getAccount(Client client) {
        io.appwrite.services.Account accountClient = new io.appwrite.services.Account(client);
        MonoContinuation<Account> accountMonoContinuation = new MonoContinuation<>();
        accountClient.get(accountMonoContinuation);
        return Mono.create(accountMonoContinuation::setMonoSink);
    }

    @SneakyThrows
    private Mono<Boolean> isAdmin(Client client) {
        Teams teamsClient = new Teams(client);
        MonoContinuation<TeamList> continuation = new MonoContinuation<>();
        teamsClient.list(continuation);
        return Mono.create(continuation::setMonoSink)
                .map(teamList -> teamList.getTeams().stream().anyMatch(t -> t.getId().equals(ADMIN_TEAM_ID)));
    }
}
