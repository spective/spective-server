package com.alexk8s.spective.server.controller;

import com.alexk8s.spective.server.repository.MediaMetadataRepository;
import com.alexk8s.spective.server.service.ContentService;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnailator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@RequestMapping("/v1/content")
@Profile("server")
@Slf4j
public class ContentController {

    private final ContentService contentService;
    private final MediaMetadataRepository mediaMetadataRepository;
    private final Scheduler preloadScheduler = Schedulers.newBoundedElastic(10, 1000, "preload-thread", 60);

    private final String preloadPath;
    private final String hashedImagePath;

    public ContentController(ContentService contentService, MediaMetadataRepository mediaMetadataRepository, @Value("${CONTENT_PRELOAD_PATH:/tmp/spective_preload}") String preloadPath, @Value("${CONTENT_HASHED_IMAGE_PATH:/tmp/spective_images}") String hashedImagePath) {
        this.contentService = contentService;
        this.mediaMetadataRepository = mediaMetadataRepository;
        this.preloadPath = preloadPath;
        this.hashedImagePath = hashedImagePath;
    }

    @GetMapping(value = "preload/{hash}", produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<Resource> getPreloaded(@PathVariable String hash, @RequestParam(required = false, defaultValue = "-1") int height) {
        return Mono.just(hash)
                .publishOn(preloadScheduler)
                .map(h -> resolvePreloadPath(h, height))
                .flatMap(p -> verifyAndUpdatePreloadExistence(hash, p))
                .map(FileSystemResource::new);
    }

    @GetMapping(value={"/hashed/{hash}","/hashed/{hash}.jpeg","/hash/{hash}", "/hash/{hash}.jpeg"}, produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<Resource> getHashed(@PathVariable String hash) {
        String path = hashedImagePath + "/" + hash;
        return Mono.just(new FileSystemResource(path));
    }

    private Mono<String> verifyAndUpdatePreloadExistence(String hash, String path) {
        if (Files.exists(Path.of(path)))
            return Mono.just(path);
        return Flux.concat(mediaMetadataRepository.findByHash(hash), mediaMetadataRepository.findByImageHash(hash))
                .map(x -> {
                    log.info("Marking {} as not preloaded since file was missing from {}", x, path);
                    x.setPreloaded(false);
                    return x;
                })
                .flatMap(mediaMetadataRepository::save)
                .then(Mono.empty());

    }


    private String resolvePreloadPath(String hash, int height) {
        log.info("Resolving preload for {} @ {}", hash, height);
        String hashPath = hash.charAt(0) + "/" + hash.substring(0, 2) + "/" + hash;
        String path = preloadPath + "/" + hashPath;
        if (height > 0) {
            try {
                path = getResized(path, height, hash);
            } catch (IOException e) {
                log.error("Error resizing image {}", hashPath, e);
            }
        }
        log.info("Resolved path {}", path);
        return path;
    }

    private String getResized(String path, int height, String hash) throws IOException {
        String resizedPath = preloadPath + "_resize/" + hash + "_" + height;
        if (Files.exists(Path.of(resizedPath))) {
            log.debug("Preload ({}) already existed", resizedPath);
            return resizedPath;
        }
        BufferedImage bufferedImage = ImageIO.read(new File(path));
        if (bufferedImage == null || bufferedImage.getHeight() < height)
            return path;
        int width = bufferedImage.getWidth() * height / bufferedImage.getHeight();
        log.debug("Resizing {} to {}x{}", path, width, height);
        BufferedImage thumbnail = Thumbnailator.createThumbnail(bufferedImage, width, height);
        if (ImageIO.write(thumbnail, "JPEG", new File(resizedPath))) {
            return resizedPath;
        } else {
            log.info("Failed to write as JPEG, trying to change image type to INT_RGB");
            BufferedImage rbgImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            rbgImage.createGraphics().drawImage(thumbnail, 0, 0, width, height, null);
            if (ImageIO.write(rbgImage, "JPEG", new File(resizedPath)))
                return resizedPath;
        }
        log.warn("No writer found for JPEG, trying to write thumbnail as PNG");
        if (ImageIO.write(thumbnail, "PNG", new File(resizedPath)))
            return resizedPath;
        log.warn("Failed to write as PNG, using original image");
        return path;
    }

    @GetMapping("saved/{fileName}")
    public Resource getSaved(@PathVariable String fileName, @RequestParam(required = false) String fallback) {
        String basePath = "/media/spective/spective/images/";
        String requestedPath = basePath + fileName;
        if (Files.exists(Path.of(requestedPath))) {
            return new FileSystemResource(requestedPath);
        }
        String fallbackPath = basePath + fallback;
        return new FileSystemResource(fallbackPath);
    }

    @GetMapping("local/{fileName}")
    public Resource getResource(@PathVariable String fileName) {
        return new FileSystemResource(contentService.getPath(fileName));

    }
}
