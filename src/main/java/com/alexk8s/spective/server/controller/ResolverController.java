package com.alexk8s.spective.server.controller;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.ResolverConfigurationRepository;
import com.alexk8s.spective.server.resolver.MediaMetadata;
import com.alexk8s.spective.server.resolver.Resolver;
import com.alexk8s.spective.server.resolver.ResolverConfiguration;
import com.alexk8s.spective.server.resolver.ResolverFactory;
import com.alexk8s.spective.server.service.TailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/v1/resolver")
@AllArgsConstructor
@Slf4j
@Profile("server")
public class ResolverController {

    private final ResolverConfigurationRepository repository;
    private final TailService tailService;
    private final ResolverFactory resolverFactory;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Flux<ResolverConfiguration> getResolvers(){
        return ReactiveSecurityContextHolder.getContext().flatMapMany(context -> {
            return repository.findAll();
        });
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    public Mono<ResolverConfiguration> createResolver(@RequestBody ResolverConfiguration resolverConfiguration){
        return ReactiveSecurityContextHolder.getContext().flatMap(context -> {
            resolverConfiguration.setCreatedBy(context.getAuthentication().getPrincipal().toString());
            resolverConfiguration.setCreatedTime(ZonedDateTime.now());
            return repository.save(resolverConfiguration);
        });
    }

    @MessageMapping("resolver.test")
    public Flux<ResolverResult> test(String domain){
        log.info("Starting test for {}",domain);
        Mono<Resolver> resolver = resolverFactory.getResolver(domain);
        return Flux.zip(tailService.backwards(), resolver.cache().repeat())
                .flatMap(tuple -> resolveResult(domain,tuple.getT2(),tuple.getT1()),3);

    }

    private Mono<ResolverResult> resolveResult(String domain,Resolver resolver,RedditPostModel redditPost){
        if(!domain.equals(redditPost.getDomain()))
            return Mono.just(new ResolverResult(redditPost.getDomain(),false, Collections.emptyList(),redditPost));
        return Flux.from(resolver.resolveMetadata(redditPost))
                .collectList()
                .map(m -> new ResolverResult(domain, true, m, redditPost));
    }



    private record ResolverResult(String domain, boolean domainMatched, List<MediaMetadata> resolvedMetadata,
                                  RedditPostModel redditPost){}

}
