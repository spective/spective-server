package com.alexk8s.spective.server.controller;

import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.RedditPostRepository;
import com.alexk8s.spective.server.repository.ResolverConfigurationRepository;
import com.alexk8s.spective.server.resolver.Resolver;
import com.alexk8s.spective.server.resolver.ResolverFactory;
import com.alexk8s.spective.server.service.ContentService;
import com.alexk8s.spective.server.service.RedditService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import masecla.reddit4j.objects.RedditPost;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@Profile("server")
@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/v1/post")
public class PostController {
    private final ResolverFactory resolverFactory;
    private final RedditPostRepository redditPostRepository;
    private final RedditService redditService;
    private final ContentService contentService;

    @GetMapping("{id}")
    public Mono<RedditPostModel> getPost(@PathVariable String id) {
        return redditPostRepository.findById(id)
                .switchIfEmpty(Mono.just(id).flatMap(this::fetchPostFromSource)) //so it doesn't trigger the call to reddit right away
                .flatMap(this::mapContent)
                .checkpoint()
                .flatMap(this::mapContentMetadata)
                .checkpoint()
                ;
    }

    public Mono<RedditPostModel> mapContentMetadata(RedditPostModel redditPostModel) {
        if (redditPostModel.getMediaMetadata() == null)
            return Mono.just(redditPostModel);
        return Flux.fromIterable(redditPostModel.getMediaMetadata())
                .flatMap(contentService::applyContentMetadata)
                .checkpoint()
                .collectList()
                .map(m -> {
                    redditPostModel.setMediaMetadata(m);
                    return redditPostModel;
                });
    }

    private Mono<RedditPostModel> mapContent(RedditPostModel redditPost) {
        if (!StringUtils.hasText(redditPost.getDomain()))
            return Mono.just(redditPost);
        return resolverFactory.getResolver(redditPost.getDomain())
                .checkpoint()
                .flatMapMany(r -> r.resolveMetadata(redditPost))
                .collectList()
                .map(m -> {
                    redditPost.setMediaMetadata(m);
                    return redditPost;
                });
    }

    private Mono<RedditPostModel> fetchPostFromSource(String id) {
        List<RedditPostModel> redditPosts = redditService.getRedditPosts(List.of(id));
        if (redditPosts.isEmpty()) {
            return Mono.empty();
        }
        RedditPostModel redditPost = redditPosts.get(0);
        return redditPostRepository.save(redditPost);
    }

}
