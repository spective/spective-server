package com.alexk8s.spective.server.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("health")
@Profile("server")
public class HealthController {
    @GetMapping
    public void getHealth(){

    }
}
