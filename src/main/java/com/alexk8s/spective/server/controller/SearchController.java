package com.alexk8s.spective.server.controller;

import com.alexk8s.spective.server.model.RedditPostId;
import com.alexk8s.spective.server.model.RedditPostModel;
import com.alexk8s.spective.server.repository.RedditPostIdRepository;
import com.alexk8s.spective.server.repository.RedditPostRepository;
import com.alexk8s.spective.server.repository.ResolverConfigurationRepository;
import com.alexk8s.spective.server.resolver.*;
import com.alexk8s.spective.server.service.ContentService;
import com.alexk8s.spective.server.service.RedditService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

@Component
@Profile("server")
@Slf4j
@RestController
public class SearchController {

    private final ResolverConfigurationRepository resolverConfigurationRepository;
    private final ResolverFactory resolverFactory;
    private final RedditPostRepository redditPostRepository;
    private final ContentService contentService;
    private final RedditPostIdRepository redditPostIdRepository;
    private final RedditService redditService;
    private final Mono<Map<String,Resolver>> cachedResolvers ;

    @Autowired
    public SearchController(ResolverConfigurationRepository resolverConfigurationRepository, ResolverFactory resolverFactory, RedditPostRepository redditPostRepository, ContentService contentService, RedditPostIdRepository redditPostIdRepository, RedditService redditService) {
        this.resolverConfigurationRepository = resolverConfigurationRepository;
        this.resolverFactory = resolverFactory;
        this.redditPostRepository = redditPostRepository;
        this.contentService = contentService;
        this.redditPostIdRepository = redditPostIdRepository;
        this.redditService = redditService;
        cachedResolvers = getResolvers().cache(Duration.ofHours(1));
    }


    @MessageMapping("search.subreddit")
    public Flux<RedditPostModel> subredditSearch(List<String> subreddits) {
        return search(null,subreddits,null);
    }

    @MessageMapping("search.author")
    public Flux<RedditPostModel> authorSearch(String author) {
        return search(List.of(author),null,null);
    }

    @GetMapping("/v1/search")
    public Flux<RedditPostModel> search(@RequestParam(required = false, value = "author") List<String> authors,
                                        @RequestParam(required = false, value = "subreddit") List<String> subreddits,
                                        @RequestParam(required = false) Long after) {
        Long createdUtc = Objects.requireNonNullElse(after,System.currentTimeMillis()/1000);
        Flux<RedditPostId> authorPosts = Flux.fromIterable(cleanSearchList(authors)).flatMap(a->redditPostIdRepository.findTop10ByTypeAndValueAndCreatedUtcLessThanOrderByCreatedUtcDesc("author",a,createdUtc));
        Flux<RedditPostId> subredditPosts = Flux.fromIterable(cleanSearchList(subreddits)).flatMap(s->redditPostIdRepository.findTop10ByTypeAndValueAndCreatedUtcLessThanOrderByCreatedUtcDesc("subreddit",s,createdUtc));
        Flux<RedditPostModel> posts = Flux.concat(authorPosts, subredditPosts)
                .sort(Comparator.comparingLong(RedditPostId::getCreatedUtc).reversed())
                .take(10)
                .map(RedditPostId::getId)
                .flatMapSequential(redditPostRepository::findById)

                ;
        return cachedResolvers.flatMapMany(r->resolveMedia(r,posts));
    }

    private List<String> cleanSearchList(List<String> inputList){
        if(inputList==null)
            return Collections.emptyList();
        return inputList.stream().map(String::toLowerCase).toList();
    }

    @GetMapping("/v1/hot")
    public Flux<RedditPostModel> getHot(@RequestParam String subreddit,@RequestParam(required = false) String after){
        List<RedditPostModel> hotPosts = redditService.getHotPosts(subreddit, after);
        return cachedResolvers.flatMapMany(r->resolveMedia(r,Flux.fromIterable(hotPosts)));
    }

    private Flux<RedditPostModel> resolveMedia(Map<String, Resolver> resolvers, Flux<RedditPostModel> redditPostFlux) {
        return redditPostFlux.flatMapSequential(p -> {
            Resolver resolver = resolvers.getOrDefault(p.getDomain(), new NoopResolver());
            return resolveMedia(resolver, p);
        }, 10);
    }

    private Mono<RedditPostModel> resolveMedia(Resolver resolver, RedditPostModel redditPost) {
        return Flux.from(resolver.resolveMetadata(redditPost))
                .publishOn(Schedulers.boundedElastic())
                .flatMap(m -> contentService.applyContentMetadata(m).checkpoint().onErrorComplete()).checkpoint()
                .doOnError((e) -> log.error("Error resolving content \n{}", redditPost, e))
                .onErrorContinue((e, o) -> {
                    log.error("Error resolving content for\n{}\n{}", redditPost, o, e);
                })
                .collectList().map(m -> {
                    redditPost.setMediaMetadata(m);
                    return redditPost;
                });
    }

    private Mono<Map<String, Resolver>> getResolvers() {
        return resolverConfigurationRepository.findAll().collectList()
                .map(l -> l.stream().collect(Collectors.toMap(ResolverConfiguration::getDomain, resolverFactory::buildResolver)));
    }

    @RequiredArgsConstructor
    private static class OrderedHandler {
        private FluxSink<RedditPostModel> sink;
        private final int subredditCount;
        private final Map<String, Queue<RedditPostModel>> latestPosts = new HashMap<>();
        private boolean isComplete = false;

        public void onNext(RedditPostModel redditPostModel) {
            latestPosts.computeIfAbsent(redditPostModel.getSubreddit(), (s) -> new LinkedBlockingQueue<>()).add(redditPostModel);
            if (sink != null && latestPosts.size() == subredditCount && latestPosts.values().stream().noneMatch(Queue::isEmpty)) {
                pop();
            }
        }

        public void setSink(FluxSink<RedditPostModel> sink) {
            this.sink = sink;
            if (isComplete)
                flush();
        }

        public void flush() {
            isComplete = true;
            if (sink != null) {

                while (!latestPosts.isEmpty()) {
                    String subreddit = pop();
                    if (latestPosts.get(subreddit).isEmpty())
                        latestPosts.remove(subreddit);
                }
                if (!sink.isCancelled()) {
                    sink.complete();
                }
            }
        }

        private String pop() {
            long latestPostTime = 0;
            String latestPostSubreddit = null;
            for (Map.Entry<String, Queue<RedditPostModel>> stringQueueEntry : latestPosts.entrySet()) {
                RedditPostModel post = stringQueueEntry.getValue().peek();
                if (post != null && post.getCreatedUtc() > latestPostTime) {
                    latestPostTime = post.getCreatedUtc();
                    latestPostSubreddit = post.getSubreddit();
                }
            }
            if (latestPostSubreddit == null)
                return latestPosts.keySet().stream().findFirst().orElse(null);
            sink.next(latestPosts.get(latestPostSubreddit).poll());
            return latestPostSubreddit;
        }
    }

}
